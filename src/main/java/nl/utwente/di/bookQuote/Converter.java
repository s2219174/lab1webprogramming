package nl.utwente.di.bookQuote;

public class Converter {

    public double convert(String input) {
        try {
            double celsius = Double.parseDouble(input);
            return (celsius * 9.0 / 5) + 32;
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
